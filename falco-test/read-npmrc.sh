#!/bin/bash

echo "running $(basename $0)"

# see https://docs.npmjs.com/files/npmrc

cat .npmrc > /dev/null 2>&1  # per-project config
cat ~/.npmrc > /dev/null 2>&1  # per-user config
cat /etc/.npmrc > /dev/null 2>&1  # global config file
