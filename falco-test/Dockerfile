ARG BASE_IMAGE=registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-2.7.patched-golang-1.17-node-16.14-postgresql-12:rubygems-3.2-git-2.36-lfs-2.9-chrome-101-yarn-1.22-graphicsmagick-1.3.36
ARG BASE_TAG=''
FROM $BASE_IMAGE$BASE_TAG

ENV DEBIAN_FRONTEND noninteractive

# Install tools that are commonly used by malicious dependencies (wget, nc, ping)
# or other tools that are targeted by malciious dependencies (private ssh keys)
RUN apt-get update && apt-get install -y \
    netcat \
    wget \
    openssh-client \
	  iputils-ping \
  && rm -rf /var/lib/apt/lists/*

RUN gem install bundler

# /my-package is used to monitor the installation of a specific dependency via yarn add
# The dependency that is to be installed has to be copied into /dependency
RUN mkdir /my-package && cd /my-package && npm init -y
RUN mkdir /dependency

# /my-app is used to monitor the installation of a project via yarn install
RUN mkdir /my-app

# create a dummy ssh key. We will watch for this key being accessed.
RUN ssh-keygen -q -t rsa -N '' -f $HOME/.ssh/id_rsa

# create dummy npm config files
RUN touch /etc/npmrc $HOME/.npmrc /my-package/.npmrc

# disable yarn update check
RUN yarn config set disable-self-update-check true -g

# create dummy gem credentials file
RUN touch $HOME/.gem/credentials && chmod 0600 $HOME/.gem/credentials

RUN mkdir $HOME/falco-test
COPY . $HOME/falco-test

CMD npm --prefix /my-package install $HOME/falco-test
WORKDIR /falco-test
