# This file modifies the default rules that come with Falco in /etc/falco/falco_rules.yaml
#
# Documentation for writing custom rules is at https://falco.org/docs/rules/


- macro: consider_ssh_reads
  condition: (always_true)

# The following 2 macros will enable monitoring for script processes (python, node, ruby etc.)
# having in-/outgoing network connections.

- list: denylisted_binaries
  items: [ps, id, whoami, lsb_release, set, env, printenv]

# Exceptions to using denylisted binaries:
- rule: Execute denylisted binary
  desc: >
    denylisted binary executed
  condition: >
    spawned_process
    and container
    and proc_name_exists
    and proc.name in (denylisted_binaries)
  output: >
    denylisted binary was executed (user=%user.name program=%proc.name
    command=%proc.cmdline parent=%proc.pname gparent=%proc.aname[2] ggparent=%proc.aname[3] gggparent=%proc.aname[4] container_id=%container.id  container_name=%container.name image=%container.image.repository)
  priority: WARNING
  tags: [mitre_discovery]

# See https://docs.npmjs.com/files/npmrc
- list: npm_config_file_names
  items: [/etc/npmrc, /root/.npmrc, /root/npm-project/.npmrc] #ToDo: replace npm-project with the package name into which subject packages are installed

- list: rubygems_config_file_names
  items: [/root/.gem/credentials]

- macro: npm_config_files
  condition: >
    fd.name in (npm_config_file_names)

- macro: rubygems_config_files
  condition: >
    fd.name in (rubygems_config_file_names)

- rule: Npm config file accessed by a program other than npm
  desc: >
    Monitor access to npm config files. These can hold credentials for NPM and give control over packages.
  condition: >
    open_read
    and npm_config_files
    and proc_name_exists
    and proc.name != npm
    and proc.cmdline != "node /usr/bin/yarn install"
    and not (proc.cmdline startswith "node /usr/bin/yarn add ")
  output: >
    NPM config opened for reading (user=%user.name
    command=%proc.cmdline parent=%proc.pname file=%fd.name parent=%proc.pname gparent=%proc.aname[2] container_id=%container.id  container_name=%container.name image=%container.image.repository)
  priority: WARNING
  tags: [filesystem, mitre_credential_access]

- rule: Rubygems credentials file accessed by a program other than Bundler
  desc: >
    Monitor access to Rubygems credentials file. These credentials can give control over Ruby Gems.
  condition: >
    open_read
    and rubygems_config_files
    and proc_name_exists
    and proc.name != bundle
  output: >
    Rubygems credentials file opened for reading (user=%user.name
    command=%proc.cmdline parent=%proc.pname file=%fd.name parent=%proc.pname gparent=%proc.aname[2] container_id=%container.id  container_name=%container.name image=%container.image.repository)
  priority: WARNING
  tags: [filesystem, mitre_credential_access]

### Outbound and Inbound Connections

- macro: consider_all_outbound_conns
  condition: (always_true)

- list: allowed_outbound_destination_ipaddrs
  #      |      Digital Ocean DNS      |
  items: ['"67.207.67.2", "67.207.67.3"']

- list: allowed_outbound_destination_domains
  # items: [] # we install packages from the filesystem, no connection to registry expected
  items: [registry.npmjs.com, rubygems.org, index.rubygems.org, api.rubygems.org] # use this if connections to the registry are required

- macro: consider_all_inbound_conns
  condition: (always_true)

- list: allowed_inbound_source_domains
  items: []

# Try consider_interpreted_outbound/inbound if consider_all_outbound_conns
# creates too many false positives.
#
# - macro: consider_interpreted_outbound
#   condition: (always_true)

# - macro: consider_interpreted_inbound
#   condition: (always_true)

### Process listens on a port

- rule: process_listen
  desc: Process started listening on a port
  condition: >
    evt.type = listen and
    evt.category = net and
    container
  output: Process started listening on a port (command=%proc.cmdline connection=%fd.name user=%user.name container_id=%container.id container_name=%container.name image=%container.image.repository)
  priority: NOTICE
  tags: [network, mitre_remote_service]

### Apply all default rules only to containers

- rule: Disallowed SSH Connection
  append: true
  condition: and container

- rule: Unexpected outbound connection destination
  append: true
  condition: and container

- rule: Unexpected inbound connection source
  append: true
  condition: and container

- rule: Modify Shell Configuration File
  append: true
  condition: and container

- rule: Read Shell Configuration File
  append: true
  condition: and container

- rule: Schedule Cron Jobs
  append: true
  condition: and container

- rule: Update Package Repository
  append: true
  condition: and container

- rule: Write below binary dir
  append: true
  condition: and container

- rule: Write below monitored dir
  append: true
  condition: and container

- rule: Read ssh information
  append: true
  condition: and container

- rule: Write below etc
  append: true
  condition: and container

- rule: Write below root
  append: true
  condition: and container

- rule: Read sensitive file trusted after startup
  append: true
  condition: and container

- rule: Read sensitive file untrusted
  append: true
  condition: and container

- rule: Write below rpm database
  append: true
  condition: and container

- rule: DB program spawned process
  append: true
  condition: and container

- rule: Modify binary dirs
  append: true
  condition: and container

- rule: Mkdir binary dirs
  append: true
  condition: and container

- rule: Change thread namespace
  append: true
  condition: and container

- rule: Run shell untrusted
  append: true
  condition: and container

- rule: Launch Privileged Container
  append: true
  condition: and container

- rule: Launch Sensitive Mount Container
  append: true
  condition: and container

- rule: Launch Disallowed Container
  append: true
  condition: and container

- rule: System user interactive
  append: true
  condition: and container

- rule: Terminal shell in container
  append: true
  condition: and container

- rule: System procs network activity
  append: true
  condition: and container

- rule: Program run with disallowed http proxy env
  append: true
  condition: and container

- rule: Interpreted procs inbound network activity
  append: true
  condition: and container

- rule: Interpreted procs outbound network activity
  append: true
  condition: and container

- rule: Unexpected UDP Traffic
  append: true
  condition: and container

- rule: Non sudo setuid
  append: true
  condition: and container

- rule: User mgmt binaries
  append: true
  condition: and container

- rule: Create files below dev
  append: true
  condition: and container

- rule: Contact EC2 Instance Metadata Service From Container
  append: true
  condition: and container

- rule: Contact cloud metadata service from container
  append: true
  condition: and container

- rule: Contact K8S API Server From Container
  append: true
  condition: and container

- rule: Unexpected K8s NodePort Connection
  append: true
  condition: and container

- rule: Launch Package Management Process in Container
  append: true
  condition: and container

- rule: Netcat Remote Code Execution in Container
  append: true
  condition: and container

- rule: Launch Suspicious Network Tool in Container
  append: true
  condition: and container

- rule: Launch Suspicious Network Tool on Host
  append: true
  condition: and container

- rule: Search Private Keys or Passwords
  append: true
  condition: and container

- rule: Clear Log Activities
  append: true
  condition: and container

- rule: Remove Bulk Data from Disk
  append: true
  condition: and container

- rule: Delete or rename shell history
  append: true
  condition: and container

- rule: Delete Bash History
  append: true
  condition: and container

- rule: Set Setuid or Setgid bit
  append: true
  condition: and container

- rule: Create Hidden Files or Directories
  append: true
  condition: and container

- rule: Launch Remote File Copy Tools in Container
  append: true
  condition: and container

- rule: Create Symlink Over Sensitive Files
  append: true
  condition: and container

- rule: Detect outbound connections to common miner pool ports
  append: true
  condition: and container

- rule: Detect crypto miners using the Stratum protocol
  append: true
  condition: and container

- rule: The docker client is executed in a container
  append: true
  condition: and container

- rule: Packet socket created in container
  append: true
  condition: and container
