'use strict' /* eslint-env mocha */

const Unpacker = require('../src/unpacker.js')

const sinon = require('sinon')

describe('Unpacker Test', function () {
  describe('unpack', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(function () {
      this.archivePath = '/path/to/archive'
      this.dstPath = '/dst/path'
      this.tmpDir = '/tmp/directory/1234'
      this.unpacker = new Unpacker(this.archivePath, this.dstPath)

      this.tarStub = sandbox.stub(this.unpacker._tar, 'extract')
      sandbox.stub(this.unpacker._fs, 'mkdtemp').returns(this.tmpDir)
      this.fsAccessStub = sandbox.stub(this.unpacker._fs, 'access')
      this.fsMoveStub = sandbox.stub(this.unpacker._fs, 'move')
    })

    afterEach(function () {
      sandbox.restore()
    })

    it('unpacks an archive into temp dir', async function () {
      this.fsAccessStub.returns(false)
      await this.unpacker.unpack()
      sinon.assert.calledWith(this.tarStub, {
        file: this.archivePath,
        'strip-components': 1,
        cwd: this.tmpDir,
        keep: true,
        unlink: true
      })
    })

    it('passes custom options to the extract function', async function () {
      this.fsAccessStub.returns(false)
      const myopts = { myopts: 1234 }
      await this.unpacker.unpack(myopts)
      sinon.assert.calledWithMatch(this.tarStub, myopts)
    })

    it('moves archive content from temp dir to destination dir', async function () {
      this.fsAccessStub.throws(() => {
        const e = new Error()
        e.code = 'ENOENT'
        return e
      })
      await this.unpacker.unpack()
      sinon.assert.calledWith(this.fsMoveStub, this.tmpDir, this.dstPath)
    })

    it('moves archive content from temp dir to destination dir and removes package folder if present', async function () {
      this.fsAccessStub.returns(false)
      await this.unpacker.unpack()
      sinon.assert.calledWith(this.fsMoveStub, this.tmpDir + '/package', this.dstPath)
    })
  })
})
