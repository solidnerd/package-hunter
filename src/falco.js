'use strict'

const AlertFilter = require('./alert-filter')
const FalcoClient = require('./falco-client')
const debug = require('debug')('pkgs:falco-client')

const opts = require('./opts')

function setupFalcoListener () {
  const falco = new FalcoClient()
  const falcoStream = falco.subscribe()

  falcoStream.on('data', function (alert) {
    const conId = alert.output_fields && alert.output_fields['container.id']
    debug(`received an alert for container ${conId}`)
    if (!conId) {
      debug(`Received alert without container id: ${alert}`)
      return
    }

    const reqId = opts.pendingContainer[conId]
    if (!reqId) {
      debug(`Could not find request id for container with id ${conId}`)
      return
    }

    const filter = new AlertFilter(alert)
    if (filter.isDiscardable()) {
      debug(`Discarding alert: ${alert.output}`)
      return
    }

    opts.jobStore[reqId].result.push(alert)
  })

  falcoStream.on('end', _end => {
    debug('falco client connection unexpectedly closed')
    clearInterval(streamWriteTicker)
    setupFalcoListener()
  })

  falcoStream.on('error', err => {
    debug('falco client encountered an error: ' + err)
  })

  falcoStream.on('status', function (status) {
    debug('falco status: %O', status)
  })

  // Write an empty message to the stream every 100ms in order to receive events.
  // It's currently unclear why this is needed, but it's assumed to be needed to tell Falco
  // That a process is subscribed and ready to accept events. If this assumption is wrong, please
  // don't hesitate to open an Issue or MR on Package Hunter! :)
  const streamWriteTicker = setInterval(() => {
    falcoStream.write({ data: {} })
  }, 100)
}

module.exports = setupFalcoListener
