'use strict'

const debug = require('debug')('pkgs:server')
const bodyParser = require('body-parser')
const express = require('express')
const mkdirp = require('mkdirp')

const printBanner = require('./banner')
const initSandboxManager = require('./init-sandbox-manager')
const opts = require('./opts')
const getOptsMiddleware = require('./opts-middleware')
const setupRoutes = require('./route')

const PORT = 3000

// expressjs config
const app = express()

const sandboxManager = initSandboxManager(app)

app.use(bodyParser.raw({
  type: 'application/octet-stream',
  limit: '200mb' // size limit of body
}))

app.get('/', getOptsMiddleware)

app.use('/monitor', setupRoutes(sandboxManager.middlewareFactory))

sandboxManager.setupEventListener()

mkdirp(opts.workdir)

app.listen(PORT, () => {
  debug(`Dependency Monitor listening at http://localhost:${PORT}`) // DevSkim: ignore DS137138
  printBanner()
})
