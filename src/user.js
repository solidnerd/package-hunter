'use strict'

const conf = require('./config')

class User {
  constructor (name, hash) {
    this.name = name
    this.hash = hash
  }

  static getAllUsers (store = conf.defaultstore) {
    if (!store) return []
    return store.map(x => new User(x.name, x.hash))
  }
}

module.exports = User
