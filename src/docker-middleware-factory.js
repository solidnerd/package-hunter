'use strict'

const cleanupMiddleware = require('./cleanup-middleware.js')
const createHandleBodyMiddleware = require('./handle-body-middleware.js')
const createFileExistsMiddleware = require('./file-exists-middleware.js')
const createBundlerMiddleware = require('./bundler-middleware.js')
const createDockerMiddleware = require('./docker-middleware.js')
const opts = require('./opts.js')

const constants = require('./constants')
const util = require('./util')

module.exports = function (mode, packageManager) {
  const bodyHandlerOpts = { unpackOpts: { 'strip-components': mode === 'dependency' ? 0 : 1 } }
  const handleBodyMiddleware = createHandleBodyMiddleware(opts.jobStore, opts.workdir, bodyHandlerOpts)

  const { manifestFile, runnerOpts } = constants.HUNTER_MANIFESTS[mode][packageManager]
  const fileExistMiddleware = createFileExistsMiddleware(manifestFile)

  const containerOpts = util.createDockerContainerOpts(runnerOpts)
  const dockerMiddleware = createDockerMiddleware(opts.jobStore, opts.pendingContainer, containerOpts)

  if (mode === 'dependency') {
    return [cleanupMiddleware, handleBodyMiddleware, dockerMiddleware]
  }

  if (packageManager === 'bundler') {
    return [cleanupMiddleware, handleBodyMiddleware, fileExistMiddleware, createBundlerMiddleware(), dockerMiddleware]
  }

  return [cleanupMiddleware, handleBodyMiddleware, fileExistMiddleware, dockerMiddleware]
}
