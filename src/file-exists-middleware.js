'use strict'

const fs = require('fs')
const path = require('path')
const debug = require('debug')('pkgs:file-exists-middleware')

module.exports = function (filename) {
  return async function (req, res, next) {
    try {
      await fs.promises.stat(path.join(req.extractDestinationPath, filename))
      next()
    } catch (err) {
      debug(err)
      if (err.code === 'ENOENT') {
        res.status(400).json({
          status: 'error',
          reason: `not a valid project: file ${filename} does not exist`
        })
      } else {
        res.status(500).json({
          status: 'error',
          reason: 'unknown error'
        })
        throw err
      }
    }
  }
}
