'use strict'

const { v4: uuidv4 } = require('uuid')
const path = require('path')
const util = require('util')
const fs = require('fs')

const debug = require('debug')('pkgs:handle-body-middleware')

const DependencyUnpacker = require('./unpacker.js')

const writeFile = util.promisify(fs.writeFile)

module.exports = function (store, workdir, opts) {
  if (!store) { throw new Error('Required parameter store is missing') }
  if (!workdir) { throw new Error('Required parameter workdir is missing') }

  const _newEntry = function () {
    const id = uuidv4()
    store[id] = {
      status: 'pending',
      id: id,
      result: []
    }
    return id
  }

  return async function (req, res, next) {
    if (!req.body) {
      res.statusCode = 400
      res.json({ status: 'error', reason: 'expected package to analyze in POST body' })
      return
    }

    const id = _newEntry()
    const tmpPath = path.join(workdir, id + '.tgz')
    debug(`writing request body to ${tmpPath}`)
    await writeFile(tmpPath, req.body)

    const extractDestinationPath = path.join(workdir, id)

    // make values available to later middlewares
    req.id = id
    req.tmpPath = tmpPath
    req.extractDestinationPath = extractDestinationPath

    const unpacker = new DependencyUnpacker(tmpPath, extractDestinationPath)
    try {
      await unpacker.unpack(opts.unpackOpts)
      next()
    } catch (err) {
      debug(err)
      res.statusCode = 400
      res.json({ status: 'error', reason: 'error unpacking the archive' })
    }
  }
}
